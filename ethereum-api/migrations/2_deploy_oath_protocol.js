var OathProtocolAgent = artifacts.require("OathProtocolAgent");
var OathProtocolContract = artifacts.require("OathProtocolContract");
var OathProtocolResolver = artifacts.require("OathProtocolResolver");

module.exports = function(deployer) {
  deployer.deploy(OathProtocolResolver);
  deployer.deploy(OathProtocolContract);
  deployer.deploy(OathProtocolAgent);
};
