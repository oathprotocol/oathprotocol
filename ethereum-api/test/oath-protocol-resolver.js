var OathProtocolResolver = artifacts.require("OathProtocolResolver");
var OathProtocolContract = artifacts.require("OathProtocolContract");

contract('OathProtocolResolver test', async (accounts) => {

  let resolverInstance, resolverContract, resolverOwner;

  beforeEach('setup contract for each test', async function () {
    resolverInstance = await OathProtocolResolver.deployed();
    resolverContract = resolverInstance.contract;
    resolverOwner = await resolverContract.owner();
  });

  it("sets contract creator as owner", async () => {
    assert.equal(resolverOwner, accounts[0]);
  });

  it("allows owner transfer ownership", async () => {
    let newOwner = accounts[2];
    await resolverContract.transferOwnership(
      newOwner,
      { from: resolverOwner }
    );
    assert.equal(resolverContract.owner(), newOwner);
  });

  it("disallows non-owner transfer ownership", async () => {
    let malicious = accounts[2];
    try {
      await resolverContract.transferOwnership(newOwner, { from: malicious });
      assert.ok(
        false,
        "Should throw exception while non-owner calls transferOwnership");
    } catch (error) {
      assert.ok(
        true,
        "Expect exception when non-owner calls transferOwnership"
      );
    }
  });

  it("allows owner set oath protocol agent", async () => {
    await resolverContract.setOathProtocolAgent(
      accounts[9],
      { from: resolverOwner }
    );
    let oathProtocolAgent = await resolverContract.oathProtocolAgent();
    assert.equal(accounts[9], oathProtocolAgent);
  });

  it("disallows non-owner set oath protocol agent", async () => {
    let malicious = accounts[2];
    try {
      await resolverContract.setOathProtocolAgent(
        accounts[9],
        { from: malicious }
      );
      assert.ok(
        false,
        "Should throw exception when non-owner calls setOathProtocolAgent"
      );
    } catch (error) {
      assert.ok(
        true,
        "Expect exception when non-owner calls setOathProtocolAgent"
      );
    }
  });

  it("panics when get agent and it is not set", async () => {
    try {
      await resolverContract.getOathProtocolAgent({ from: resolverOwner });
      assert.ok(
        false,
        "Should throw exception when contract address is not set"
      );
    } catch (error) {
      assert.ok(
        true,
        "Expect exception when contract address is not set"
      );
    }
  });

  it("returns agent if it is set", async () => {
    let protocol = await OathProtocolContract.deployed();
    let protocolContract = protocol.contract;
    await resolverContract.setOathProtocolAgent(
      protocolContract.address,
      { from: resolverOwner }
    );
    let resolved = await resolverContract.getOathProtocolAgent.call(
      { from: resolverOwner }
    );
    assert.equal(resolved, protocolContract.address);
  });

});
