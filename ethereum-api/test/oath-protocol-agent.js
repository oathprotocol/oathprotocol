var OathProtocolAgent = artifacts.require("OathProtocolAgent");
var truffleAssert = require('truffle-assertions');

contract('OathProtocolAgent test', async (accounts) => {

  let disputeContract;
  let agentInstance, agentContract, agentOwner;
  let oathCallback, prosecutor, defendant;

  beforeEach('setup contract for each test', async function () {
    agentInstance = await OathProtocolAgent.deployed();
    agentContract = agentInstance.contract;
    agentOwner = await agentContract.owner();
    oathCallback = accounts[7];
    prosecutor = accounts[8];
    defendant = accounts[9];
    // Creates another OathProtocolAgent as dispute contract.
    let disputeInstance = await OathProtocolAgent.deployed();
    disputeContract = disputeInstance.contract;
  });

  it("allows set oath callback by owner", async () => {
    let newOathCallback = accounts[6];
    assert.notEqual(newOathCallback, oathCallback);
    await agentContract.setOathCallback(
      newOathCallback,
      { from: agentOwner }
    );
    let actualOathCallback = await agentContract.oathCallback();
    assert.equal(newOathCallback, actualOathCallback);
  });

  it("disallows set oath callback by non-owner", async () => {
    let newOathCallback = accounts[6];
    assert.notEqual(newOathCallback, oathCallback);
    try {
      await agentContract.setOathCallback(
        newOathCallback,
        { from: newOathCallback }
      );
      assert.ok(false, "Should throw exception if not called by owner");
    } catch (error) {
      assert.ok(true, "Expect exception if not called by owner");
    }
  });

  it("allows create dispute", async () => {
    let tx = await agentContract.createDispute(
      prosecutor,
      defendant,
      { from: accounts[2] }
    );
    assert.isNotEmpty(tx);
  });

  it("emits onDisputeCreated after create dispute", async () => {
    let tx = await agentInstance.createDispute(
      prosecutor,
      defendant,
      { from: accounts[2] }
    );
    truffleAssert.eventEmitted(tx, 'DisputeFiled', (ev) => {
      return ev.disputeContract === accounts[2]
      && ev.prosecutor === prosecutor
      && ev.defendant === defendant;
    });
  });

  it("allows onDisputeCreated from oath callback", async () => {
    await agentContract.setOathCallback(
      oathCallback,
      { from: agentOwner }
    );
    let disputeId = "disputeId";
    let tx = await agentContract.onDisputeCreated(
      disputeId,
      disputeContract.address,
      { from: oathCallback }
    );
    assert.isNotEmpty(tx);
  });

  it("disallows onDisputeCreated from non oath callback", async () => {
    await agentContract.setOathCallback(
      oathCallback,
      { from: agentOwner }
    );
    let disputeId = "disputeId";
    try {
      await agentContract.onDisputeCreated(
        disputeId,
        { from: accounts[3] }
      );
      assert.ok(false, "Should throw exception on unauthorized caller");
    } catch (error) {
      assert.ok(true, "Expect exception on unauthorized caller");
    }
  });

  it("allows onDisputeResolved from oath callback", async () => {
    await agentContract.setOathCallback(
      oathCallback,
      { from: agentOwner }
    );
    let disputeId = "disputeId";
    let tx = await agentContract.onDisputeResolved(
      disputeId,
      disputeContract.address,
      1, /* Ruling.Tie */
      { from: oathCallback }
    );
    assert.isNotEmpty(tx);
  });

  it("disallows onDisputeResolved from non oath callback", async () => {
    await agentContract.setOathCallback(
      oathCallback,
      { from: agentOwner }
    );
    let disputeId = "disputeId";
    try {
      await agentContract.onDisputeResolved(
        disputeId,
        disputeContract.address,
        1, /* Ruling.Tie */
        { from: accounts[3] }
      );
      assert.ok(false, "Should throw exception on unauthorized caller");
    } catch (error) {
      assert.ok(true, "Expect exception on unauthorized caller");
    }
  });

});
