pragma solidity ^0.4.24;


import "./stub/IOathProtocolContract.sol";
import "./stub/IOathProtocolResolver.sol";


/**
 * @dev Implementation of IOathProtocolResolver.
 */
contract OathProtocolResolver is IOathProtocolResolver {

    address public owner = msg.sender;

    address public oathProtocolAgent;

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    function setOathProtocolAgent(address _address) public onlyOwner {
        oathProtocolAgent = _address;
    }

    function transferOwnership(address newOwner) public onlyOwner {
        require(newOwner != address(0), "Can not transfer ownership to 0x0");
        owner = newOwner;
    }

    function getOathProtocolAgent() external returns(IOathProtocolContract) {
        require(
            oathProtocolAgent != address(0),
            "OATH Protocol agent is not initialized in current network"
        );
        return IOathProtocolContract(oathProtocolAgent);
    }
}
