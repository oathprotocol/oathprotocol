pragma solidity ^0.4.24;


import "./IOathProtocolContract.sol";


/**
 * @dev Interface to obtain IOathProtocolContract agent in current network.
 */
interface IOathProtocolResolver {
    /**
     * @return IOathProtocolContract agent in current network.
     */
    function getOathProtocolAgent() external returns(IOathProtocolContract);
}
