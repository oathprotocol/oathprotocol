pragma solidity ^0.4.24;


/**
 * @dev OathProtocol API abstract contract.
 * This supposes to be an Interface except that Solidity interface
 * does not allow nested enum type.
 */
contract IOathProtocolContract {

    enum Ruling {
        Unknown,
        Tie,
        LeaningProsecutor,
        LeaningDefendant
    }

    /**
     * @dev Event emitted after filing a dispute.
     * OATH Protocol listens for this event filed from agent.
     */
    event DisputeFiled(
        address disputeContract,
        address prosecutor,
        address defendant
    );

    /**
     * @dev Create a new dispute in OATH Protocol.
     * This is called by integrated contract to agent and result
     * will be delivered via onDisputeCreated callback.
     */
    function createDispute(
        address prosecutor,
        address defendant
    )
        external
        payable;

    /**
     * @dev Callback after a dispute is created in OATH Protocol.
     * OATH Protocol issues this callback to agent, dispute result
     * will be delivered via onDisputeResolved callback.
     */
    function onDisputeCreated(
        string disputeId,
        address disputeContract
    )
        external;

    /**
     * @dev Callback after a dispute is resolved in OATH Protocol.
     * OATH Protocol issues this callback to agent.
     */
    function onDisputeResolved(
        string disputeId,
        address disputeContract,
        Ruling ruling
    )
        external;

}
