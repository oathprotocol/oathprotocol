pragma solidity ^0.4.24;


import "./stub/IOathProtocolContract.sol";
import "./stub/IOathProtocolResolver.sol";


/**
 * @dev Extend this base contract to integrate with OATH Protocol.
 */
contract OathProtocolContract is IOathProtocolContract {

    /**
     * @dev Mapping from network id to IOathProtocolResolver
     * TODO: Deploy resolvers and have the mapping here.
     */
    mapping (uint => address) internal resolvers;

    IOathProtocolResolver internal oathProtocolResolver;

    enum Stage {
        Unknown,
        DisputeFiled,
        DisputeCreated,
        DisputeResolved
    }

    Stage public stage = Stage.Unknown;

    address public owner = msg.sender;

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    modifier fromOathAgent {
        require(oathProtocolResolver != address(0));
        IOathProtocolContract oathProtocolAgent =
            oathProtocolResolver.getOathProtocolAgent();
        require(msg.sender == address(oathProtocolAgent));
        _;
    }

    modifier atStage(Stage _stage) {
        require(stage == _stage);
        _;
    }

    modifier requireOathAgent() {
        if (oathProtocolResolver == address(0)) {
            oathProtocolResolver = getOathProtocolResolver();
            require(oathProtocolResolver != address(0));
            require(oathProtocolResolver.getOathProtocolAgent() != address(0));
        }
        _;
    }

    function createDispute(
        address prosecutor,
        address defendant
    )
        external
        atStage(Stage.Unknown)
        requireOathAgent
        payable {
        stage = Stage.DisputeFiled;
        oathProtocolResolver.getOathProtocolAgent().createDispute(
            prosecutor,
            defendant);
    }

    function onDisputeCreated(
        string disputeId,
        address disputeContract 
    )
        external
        atStage(Stage.DisputeFiled)
        fromOathAgent {
        stage = Stage.DisputeCreated;
        // silence compilation warnings.
        disputeId; disputeContract;
    }

    function onDisputeResolved(
        string disputeId,
        address disputeContract,
        Ruling ruling
    )
        external
        atStage(Stage.DisputeCreated)
        fromOathAgent {
        stage = Stage.DisputeResolved;
        // silence compilation warnings.
        disputeId; disputeContract; ruling;
    }

    function getOathProtocolResolver() internal returns(IOathProtocolResolver) {
        // TODO: find oathProtocolResolver in current network
        return IOathProtocolResolver(address(0));
    }
}
