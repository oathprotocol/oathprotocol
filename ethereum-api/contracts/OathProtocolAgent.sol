pragma solidity ^0.4.24;


import "./stub/IOathProtocolContract.sol";


/**
 * @dev IOathProtocolContract agent sits in between smart contract
 * and OATH Protocol infrastructure.
 */
contract OathProtocolAgent is IOathProtocolContract {

    address public owner = msg.sender;

    address public oathCallback;

    modifier onlyOwner {
        require(msg.sender == owner);
        _;
    }

    modifier fromOathCallback {
        require(
            oathCallback != address(0) &&
            msg.sender == oathCallback
        );
        _;
    }

    function setOathCallback(address callback) external onlyOwner {
        oathCallback = callback;
    }

    /**
     * @dev msg.sender is an integrated OathProtocolContract.
     */
    function createDispute(
        address prosecutor,
        address defendant
    )
        external
        payable {
        emit DisputeFiled(msg.sender, prosecutor, defendant);
    }

    function onDisputeCreated(
        string disputeId,
        address disputeContract
    )
        external
        fromOathCallback {
        // silence compilation warnings.
        disputeId; disputeContract;
    }

    function onDisputeResolved(
        string disputeId,
        address disputeContract,
        Ruling ruling
    )
        external
        fromOathCallback {
        // silence compilation warnings.
        disputeId; disputeContract; ruling;
    }
}
