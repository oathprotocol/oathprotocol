require("dotenv").load();
var HDWalletProvider = require("truffle-hdwallet-provider");

/**
 * See <http://truffleframework.com/docs/advanced/configuration>
 */
module.exports = {
  networks: {
    ganache: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*"
    },
    ropsten: {
      provider: () =>
        new HDWalletProvider(process.env.MNEMONIC_ROPSTEN, "https://ropsten.infura.io/"),
      network_id: "3",
    }
  }
};
