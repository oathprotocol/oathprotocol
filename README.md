# OathProtocol

OATH’s dispute resolution protocol is modeled on the common-law jury system and utilizes blockchain, cryptographic algorithms, random algorithms associated with categories and attributes, jurors’ credit level, and case-tracking technology. OATH is building a decentralized, standard, and extensible public chain-agnostic protocol that protects dApp users’ rights and assets.

See more [OATH Website](https://oaths.io/)
