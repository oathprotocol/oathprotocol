let fs = require("fs");
let Web3 = require('web3');
let web3;

if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:9545/"));
}

// load the smart contract abi from json file
let input = fs.readFileSync('ethereum-api/build/contracts/OathProtocolAgent.json');
var parsed= JSON.parse(input);
var abi = parsed.abi;

//========================
// Event Listener
//========================

var latestBlockNumber = 0;
var transactionHashList = [];

let minutes = 0.1, the_interval = minutes * 60 * 1000;
setInterval(function() {
  console.log("---------------I am doing my 0.1 minutes check-------------");
  startListener('0xf204a4ef082f5c04bb89f7d5e6568b796096735a', 'DisputeFiled');
}, the_interval);

function startListener(contractAddress, eventName) {
	let myContract = new web3.eth.Contract(abi, contractAddress);
    myContract.getPastEvents(eventName, {
    	fromBlock: latestBlockNumber,
    	toBlock: 'latest'
	}, function(error, events){ 
		// console.log(events); 
	})
	.then(function(events){
		if (events.length > 0) {
			latestBlockNumber = events[events.length-1].blockNumber;
			console.log('latest block number is: ' + latestBlockNumber);
			var newTransactionHastList = [];
			events.forEach(function(item, index, array) {
				if (transactionHashList.indexOf(item.transactionHash) == -1) {
					console.log('===================== Catch >>> ' + eventName + ' <<< Event =========================');
  					console.log('BlockNumber: ' + item.blockNumber + ' || Transaction Hash: ' +  item.transactionHash);
  					console.log('Dispute Contract: ' + item.returnValues.disputeContract);
					console.log('prosecutor: ' + item.returnValues.prosecutor);
					console.log('defendant: ' + item.returnValues.defendant);
  					newTransactionHastList.push(item.transactionHash);
				}
			});

			if (newTransactionHastList.length > 0) {
				console.log('==============================================');
    			console.log('all new transactionHash: ' + newTransactionHastList.toString() + '\r\n');
				transactionHashList = transactionHashList.concat(newTransactionHastList);
			} else {
				console.log('No new events, latest block number is: ' + latestBlockNumber + '\r\n');
			}
		} else {
			transactionHashList = [];
			console.log('No new events, latest block number is: ' + latestBlockNumber + '\r\n');
		}
		
	});
}
