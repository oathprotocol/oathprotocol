let fs = require("fs");
let solc = require('solc')
let Web3 = require('web3');
let web3;

if (typeof web3 !== 'undefined') {
    web3 = new Web3(web3.currentProvider);
} else {
    // set the provider you want from Web3.providers
    web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:9545/"));
}

// load the smart contract abi from json file
let input = fs.readFileSync('ethereum-api/build/contracts/OathProtocolAgent.json');
var parsed= JSON.parse(input);
var abi = parsed.abi;

// change
let contractAddress = '0xf204a4ef082f5c04bb89f7d5e6568b796096735a';

createDispute();

function createDispute() {
	console.log("starting createDispute on contract");

	// getting the address directly from current eth chain
	web3.eth.getAccounts().then(function(addresses){
		var walletAddress = addresses[0];
		console.log (walletAddress);

		//get the smart contract from abi and the address
		let myContract = new web3.eth.Contract(abi, contractAddress, {gasPrice: '12345678', from: walletAddress});

		// // 1. user transaction to trigger the createDispute Method
		// let query = myContract.methods.createDispute('0xf17f52151ebef6c7334fad080c5704d77216b732', '0xc5fdf4076b8f3a5357c5e395ab970b5b54098fef');
		// let encodedABI = query.encodeABI();
		// let tx = {
		// from: walletAddress,
		// to: contractAddress,
		// gas: 2000000,
		// data: encodedABI,
		// };

		// // hard coded private key for the first account
		// // change this accordingly. for testing purpose only
		// var key = 'c87509a1c067bbde78beb793e6fa76530b6382a4c0241e5e4a9ec0a0f44dc0d3';


		// // check if that private key is linking to the right account
		// const account = web3.eth.accounts.privateKeyToAccount('0x' + key);
		// console.log(account);

		// //check if that account has enough eth
		// web3.eth.getBalance(walletAddress).then(console.log);
		
		// web3.eth.accounts.signTransaction(tx, '0x' + key).then(signed => {
		// const tran = web3.eth
		// 	.sendSignedTransaction(signed.rawTransaction)
		// 	.on('confirmation', (confirmationNumber, receipt) => {
		// 		console.log('=> confirmation: ' + confirmationNumber);
		// 	})
		// 	.on('transactionHash', hash => {
		// 		console.log('=> hash');
		// 		console.log(hash);
		// 	})
		// 	.on('receipt', receipt => {
		// 		console.log('=> reciept');
		// 		console.log(receipt);
		// 	})
		// 	.on('error', console.error);
		// });

		//2. 这个是直接call method.
		myContract.methods.createDispute('0xf17f52151ebef6c7334fad080c5704d77216b732', '0xc5fdf4076b8f3a5357c5e395ab970b5b54098fef')
		.send()
		.on('confirmation', (confirmationNumber, receipt) => {
			console.log('=> confirmation: ' + confirmationNumber);
		})
		.on('transactionHash', hash => {
			console.log('=> hash');
			console.log(hash);
		})
		.on('receipt', receipt => {
			console.log('=> reciept');
			console.log(receipt);
		})
		.on('error', console.error)
		.catch(console.log());
	});
}